from rest_framework import serializers, viewsets

from Restaurante.models import Client, Waiter, Table, Product, Invoice, Order


class ClientSerializer(serializers.HyperlinkedModelSerializer):
    """
    This serializer will convert the client model structure to a Json format.
    """

    class Meta:
        model = Client
        fields = (
            'url',
            'id',
            'name',
            'last_name',
            'observations',
        )


class WaiterSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Waiter
        fields = (
            'url',
            'id',
            'first_name',
            'last_name',
        )


class TableSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Table
        fields = (
            'url',
            'id',
            'number_diner',
            'location',
        )


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = (
            'url',
            'id',
            'name',
            'description',
            'imported',
        )


class InvoiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Invoice
        fields = (
            'url',
            'id',
            'date_invoice',
            'id_client',
            'id_waiter',
            'id_table',
        )


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Order
        fields = (
            'url',
            'id',
            'quantity',
            'products',
            'invoices',
        )
