"""Django_RFW URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from rest_framework import routers
from Restaurante.views import *


router = routers.DefaultRouter()
router.register(r'clients', ClientViewSet)
router.register(r'waiters', WaiterViewSet)
router.register(r'tables', TableLViewSet)
router.register(r'products', ProductViewSet)
router.register(r'invoices', InvoiceViewSet)
router.register(r'order', OrderViewSet)

urlpatterns = [
    path('listClient/', ClientListVIew.as_view(), name='ListClient'),
    path('detailclient/<int:pk>/', ClientDetailView.as_view(), name='DetailClient'),
    path('createclient/', ClientCreateView.as_view(), name='createclient'),
    path('updateteclient/<int:pk>/', ClientUpdateView.as_view(), name='updateteclient'),
    path('deleteclient/<int:pk>/', ClientDeleteView.as_view(), name='deleteclient'),

    path('listWaiter/', WaiterListVIew.as_view(), name='listWaiter'),
    path('detailWaiter/<int:pk>/', WaiterDetailView.as_view(), name='detailWaiter'),
    path('createWaiter/', WaiterCreateView.as_view(), name='createWaiter'),
    path('updateteWaiter/<int:pk>/', WaiterUpdateView.as_view(), name='updateteWaiter'),
    path('deleteWaiter/<int:pk>/', WaiterDeleteView.as_view(), name='deleteWaiter'),

    path('listTable/', TableListVIew.as_view(), name='listTable'),
    path('detailTable/<int:pk>/', TableDetailView.as_view(), name='detailTable'),
    path('createTable/', TableCreateView.as_view(), name='createTable'),
    path('updateteTable/<int:pk>/', TableUpdateView.as_view(), name='updateteTable'),
    path('deleteTable/<int:pk>/', TableDeleteView.as_view(), name='deleteTable'),

    path('listProduct/', ProductListVIew.as_view(), name='listProduct'),
    path('detailProduct/<int:pk>/', ProductDetailView.as_view(), name='detailProduct'),
    path('createProduct/', ProductCreateView.as_view(), name='createProduct'),
    path('updateteProduct/<int:pk>/', ProductUpdateView.as_view(), name='updateteProduct'),
    path('deleteProduct/<int:pk>/', ProductDeleteView.as_view(), name='deleteProduct'),

    path('listInvoice/', InvoiceListVIew.as_view(), name='listInvoice'),
    path('detailInvoice/<int:pk>/', InvoiceDetailView.as_view(), name='detailInvoice'),
    path('createInvoice/', InvoiceCreateView.as_view(), name='createProduct'),
    path('updateteInvoice/<int:pk>', InvoiceUpdateView.as_view(), name='updateteInvoice'),
    path('deleteInvoice/<int:pk>/', InvoiceDeleteView.as_view(), name='deleteInvoice'),

    path('ListOrder/', OrderListVIew.as_view(), name='ListOrder'),
    path('detailOrder/<int:pk>/', OrderDetailView.as_view(), name='detailOrder'),
    path('createOrder/', OrderCreateView.as_view(), name='createOrder'),
    path('updateteOrder/<int:pk>/', OrderUpdateView.as_view(), name='updateteOrder'),
    path('deleteOrder/<int:pk>/', OrderDeleteView.as_view(), name='OrderOrder'),



]
