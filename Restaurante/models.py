from django.db import models


# Create your models here.
class Client(models.Model):
    name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    observations = models.TextField()

    def __str__(self):
        return self.name


class Waiter(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)

    def __str__(self):
        return self.first_name


class Table(models.Model):
    number_diner = models.PositiveSmallIntegerField()
    location = models.CharField(max_length=45)

    def __str__(self):
        return self.location


class Product(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    imported = models.FloatField()

    def __str__(self):
        return self.name


class Invoice(models.Model):
    date_invoice = models.DateTimeField()
    id_client = models.ForeignKey(Client, on_delete=models.CASCADE)
    id_waiter = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    id_table = models.ForeignKey(Table, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.date_invoice} {self.id_waiter}'


class Order(models.Model):
    quantity = models.IntegerField()
    products = models.ForeignKey(Product, on_delete=models.CASCADE)
    invoices = models.ForeignKey(Invoice, on_delete=models.CASCADE)
