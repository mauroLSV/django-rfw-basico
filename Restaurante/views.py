# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from rest_framework import viewsets

from Restaurante.serializers import ClientSerializer, WaiterSerializer, TableSerializer, ProductSerializer, \
    InvoiceSerializer, OrderSerializer
from .forms import FormClient, FormWaiter, FormTable, FormProduct, FormInvoice, FormOrder
from .models import Client, Waiter, Table, Product, Invoice, Order


class ClientListVIew(ListView):
    template_name = 'cliente/ListClient.html'
    ordering = 'id'

    def get_queryset(self):
        palabra = self.request.GET.get('kword', '')
        lista = Client.objects.filter(name__icontains=palabra)
        return lista


class ClientDetailView(DetailView):
    model = Client
    template_name = 'cliente/detail_client.html'


class ClientCreateView(CreateView):
    model = Client
    template_name = 'cliente/create_client.html'
    form_class = FormClient
    success_url = reverse_lazy('listClient')


class ClientUpdateView(UpdateView):
    model = Client
    template_name = 'cliente/create_client.html'
    fields = ['name', 'last_name', 'observations']
    success_url = reverse_lazy('ListClient')


class ClientDeleteView(DeleteView):
    model = Client
    template_name = 'cliente/delete_client.html'
    success_url = reverse_lazy('ListClient')


"""
-------------------------------------------------------------------------------------------
"""


class WaiterListVIew(ListView):
    template_name = 'Waiter/list_waiter.html'
    ordering = 'id'

    def get_queryset(self):
        palabra = self.request.GET.get('kword', '')
        lista = Waiter.objects.filter(first_name__icontains=palabra)
        return lista


class WaiterDetailView(DetailView):
    model = Waiter
    template_name = 'Waiter/detail_waiter.html'


class WaiterCreateView(CreateView):
    model = Waiter
    template_name = 'Waiter/create_waiter.html'
    form_class = FormWaiter
    success_url = reverse_lazy('listWaiter')


class WaiterUpdateView(UpdateView):
    model = Waiter
    template_name = 'Waiter/create_waiter.html'
    fields = ['first_name', 'last_name']
    success_url = reverse_lazy('listWaiter')


class WaiterDeleteView(DeleteView):
    model = Waiter
    template_name = 'Waiter/delete_waiter.html'
    success_url = reverse_lazy('listWaiter')


"""
-------------------------------------------------------------------------------------------
"""


class TableListVIew(ListView):
    template_name = 'Waiter/list_Table.html'
    ordering = 'id'

    def get_queryset(self):
        palabra = self.request.GET.get('kword', '')
        lista = Table.objects.filter(number_diner__icontains=palabra)
        return lista


class TableDetailView(DetailView):
    model = Table
    template_name = 'Table/detail_Table.html'


class TableCreateView(CreateView):
    model = Table
    template_name = 'Table/create_Table.html'
    form_class = FormTable
    success_url = reverse_lazy('listTable')


class TableUpdateView(UpdateView):
    model = Table
    template_name = 'Table/create_Table.html'
    fields = ['number_diner', 'location']
    success_url = reverse_lazy('listTable')


class TableDeleteView(DeleteView):
    model = Table
    template_name = 'Table/delete_Table.html'
    success_url = reverse_lazy('listTable')


"""
-------------------------------------------------------------------------------------------
"""


class ProductListVIew(ListView):
    template_name = 'Product/List_Product.html'
    ordering = 'id'

    def get_queryset(self):
        palabra = self.request.GET.get('kword', '')
        lista = Product.objects.filter(name__icontains=palabra)
        return lista


class ProductDetailView(DetailView):
    model = Product
    template_name = 'Product/detail_Product.html'


class ProductCreateView(CreateView):
    model = Product
    template_name = 'Product/create_Product.html'
    form_class = FormProduct
    success_url = reverse_lazy('listProduct')


class ProductUpdateView(UpdateView):
    model = Product
    template_name = 'Product/create_Product.html'
    fields = ['name', 'description', 'imported']
    success_url = reverse_lazy('listProduct')


class ProductDeleteView(DeleteView):
    model = Product
    template_name = 'Product/delete_Product.html'
    success_url = reverse_lazy('listProduct')


"""
-------------------------------------------------------------------------------------------
"""


class InvoiceListVIew(ListView):
    template_name = 'Invoice/List_Invoice.html'
    ordering = 'id'

    def get_queryset(self):
        palabra = self.request.GET.get('kword', '')
        lista = Invoice.objects.filter(id_client__icontains=palabra)
        return lista


class InvoiceDetailView(DetailView):
    model = Invoice
    template_name = 'Invoice/detail_Invoice.html'


class InvoiceCreateView(CreateView):
    model = Invoice
    template_name = 'Invoice/create_Invoice.html'
    form_class = FormInvoice
    success_url = reverse_lazy('listInvoice')


class InvoiceUpdateView(UpdateView):
    model = Invoice
    template_name = 'Invoice/create_Invoice.html'
    fields = ['date_invoice', 'id_client', 'id_waiter', 'id_table']
    success_url = reverse_lazy('listInvoice')


class InvoiceDeleteView(DeleteView):
    model = Invoice
    template_name = 'Invoice/delete_Invoice.html'
    success_url = reverse_lazy('listInvoice')


"""
-------------------------------------------------------------------------------------------
"""


class OrderListVIew(ListView):
    template_name = 'Order/List_Order.html'
    ordering = 'id'

    def get_queryset(self):
        palabra = self.request.GET.get('kword', '')
        lista = Order.objects.filter(invoices__icontains=palabra)
        return lista


class OrderDetailView(DetailView):
    model = Order
    template_name = 'Order/detail_Order.html'


class OrderCreateView(CreateView):
    model = Order
    template_name = 'Order/create_Order.html'
    form_class = FormOrder
    success_url = reverse_lazy('ListOrder')


class OrderUpdateView(UpdateView):
    model = Order
    template_name = 'Order/create_Order.html'
    fields = ['quantity', 'products', 'invoices']
    success_url = reverse_lazy('ListOrder')


class OrderDeleteView(DeleteView):
    model = Order
    template_name = 'Order/delete_Order.html'
    success_url = reverse_lazy('ListOrder')


"""
-------------------------------------------------------------------------------------------
        SERIALIZERS
"""


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class WaiterViewSet(viewsets.ModelViewSet):
    queryset = Waiter.objects.all()
    serializer_class = WaiterSerializer


class TableLViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
