from django.contrib import admin

# Register your models here.
from Restaurante.models import (
    Client,
    Waiter,
    Table,
    Invoice,
    Order, Product,
)


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'last_name',
        'observations'
    )


@admin.register(Waiter)
class WaiterAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'first_name',
        'last_name'
    )


@admin.register(Table)
class TableAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'number_diner',
        'location'
    )


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'description',
        'imported',

    )


@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'date_invoice',
        'id_client',
        'id_waiter',
        'id_table',

    )
    list_editable = (
        'date_invoice',
        'id_client',
        'id_waiter',
        'id_table',
    )


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'quantity',
        'products',
        'invoices',
    )
