from django import forms

from Restaurante.models import (
    Client,
    Waiter,
    Table,
    Product,
    Invoice,
    Order
)


class FormClient(forms.ModelForm):
    class Meta:
        model = Client
        fields = (
            'name',
            'last_name',
            'observations',
        )


class FormWaiter(forms.ModelForm):
    class Meta:
        model = Waiter
        fields = (
            'first_name',
            'last_name',
        )


class FormTable(forms.ModelForm):
    class Meta:
        model = Table
        fields = (
            'number_diner',
            'location',
        )


class FormProduct(forms.ModelForm):
    class Meta:
        model = Product
        fields = (
            'name',
            'description',
            'imported',
        )


class FormInvoice(forms.ModelForm):
    date_invoice = forms.DateTimeField(widget=forms.SelectDateWidget)
    id_client = forms.ModelChoiceField(queryset=Client.objects.all())
    id_waiter = forms.ModelChoiceField(queryset=Waiter.objects.all())
    id_table = forms.ModelChoiceField(queryset=Table.objects.all())

    class Meta:
        model = Invoice
        fields = (
            'date_invoice',
            'id_client',
            'id_waiter',
            'id_table',
        )


class FormOrder(forms.ModelForm):
    quantity = forms.IntegerField()
    products = forms.ModelChoiceField(queryset=Product.objects.all())
    invoices = forms.ModelChoiceField(queryset=Invoice.objects.all())

    class Meta:
        model = Order
        fields = (
            'quantity',
            'products',
            'invoices',
        )
